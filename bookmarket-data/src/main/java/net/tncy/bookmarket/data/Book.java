package net.tncy.bookmarket.data;

import net.tncy.gma.validator.ISBN;

public class Book{
    private String title;
    private int id;
    private String author;
    private String publisher;
    private BookFormat format;
    @ISBN
    private String isbn;

    public Book() {
    }
    
    public Book(String title, int id, String author, String publisher, BookFormat format, String isbn) {
        this.title = title;
        this.id = id;
        this.author = author;
        this.publisher = publisher;
        this.format = format;
        this.isbn = isbn;
    }

    public Book title(String title) {
        setTitle(title);
        return this;
    }

    public Book id(int id) {
        setId(id);
        return this;
    }

    public Book author(String author) {
        setAuthor(author);
        return this;
    }

    public Book publisher(String publisher) {
        setPublisher(publisher);
        return this;
    }

    public Book format(BookFormat format) {
        setFormat(format);
        return this;
    }

    public Book isbn(String isbn) {
        setIsbn(isbn);
        return this;
    }


    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public BookFormat getFormat() {
        return this.format;
    }

    public void setFormat(BookFormat format) {
        this.format = format;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

}