package net.tncy.bookmarket.data;

public class InventoryEntry {
    private Book book;
    private int quantity;
    private float averagePrice;
    private float currentPrice;

    public InventoryEntry() {
    }

    public InventoryEntry(Book book, int quantity, float averagePrice, float currentPrice) {
        this.book = book;
        this.quantity = quantity;
        this.averagePrice = averagePrice;
        this.currentPrice = currentPrice;
    }

    public Book getBook() {
        return this.book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getAveragePrice() {
        return this.averagePrice;
    }

    public void setAveragePrice(float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public float getCurrentPrice() {
        return this.currentPrice;
    }

    public void setCurrentPrice(float currentPrice) {
        this.currentPrice = currentPrice;
    }

    public InventoryEntry book(Book book) {
        setBook(book);
        return this;
    }

    public InventoryEntry quantity(int quantity) {
        setQuantity(quantity);
        return this;
    }

    public InventoryEntry averagePrice(float averagePrice) {
        setAveragePrice(averagePrice);
        return this;
    }

    public InventoryEntry currentPrice(float currentPrice) {
        setCurrentPrice(currentPrice);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " book='" + getBook() + "'" +
            ", quantity='" + getQuantity() + "'" +
            ", averagePrice='" + getAveragePrice() + "'" +
            ", currentPrice='" + getCurrentPrice() + "'" +
            "}";
    }


}
